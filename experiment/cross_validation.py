'''
Crossvalidation helper module
'''
import os
from collections import deque
from scipy import ndimage
import numpy as np

from research_framework.segmentation.flair.experiment.arguments import parse

ROWS = 256
COLS = 256
CHANNELS = 1
TRAINING_SIZE = 8
VALIDATION_SIZE = 2
TESTING_SIZE = 1

class CrossValidation(object):
    '''
    Crossvalidation object to retrieve different subsets
    from a dataset
    '''
    def __init__(self, samples, labels, xfold):
        self.samples_dataset = samples
        self.labels_dataset = labels

        assert samples.get_classes() == labels.get_classes()

        classes = deque(samples.get_classes())
        classes.rotate(-xfold)
        classes = list(classes)

        assert len(classes) == TRAINING_SIZE + VALIDATION_SIZE + TESTING_SIZE

        self.training_classes = classes[:TRAINING_SIZE]
        self.validation_classes = classes[TRAINING_SIZE:TRAINING_SIZE + VALIDATION_SIZE]
        self.testing_classes = classes[TRAINING_SIZE + VALIDATION_SIZE:]

        assert len(self.training_classes) == TRAINING_SIZE
        assert len(self.validation_classes) == VALIDATION_SIZE
        assert len(self.testing_classes) == TESTING_SIZE

        self.fold_index = xfold

    def get_training_subset(self, seed=None):
        '''
        Returns a training subset according to the fold we're in
        :param seed: Random seed
        :return: Training susbet
        '''

        images = np.empty(shape=(1, ROWS, COLS, CHANNELS), dtype=np.uint8)
        masks = np.empty(shape=(1, ROWS, COLS, CHANNELS), dtype=np.uint8)

        np.random.seed(seed)

        file_names = []
        root = self.samples_dataset._get_path()
        for item in self.training_classes:
            for subdir in os.listdir(os.path.join(root, str(item))):
                file_names.append(os.path.join(str(item), subdir))

        for idx, filename in enumerate(np.random.choice(file_names, 1)):
            images[idx] = ndimage.imread(os.path.join(self.samples_dataset._get_path(), filename),
                                         'grayscale').reshape(ROWS, COLS, CHANNELS)
            masks[idx] = ndimage.imread(os.path.join(self.labels_dataset._get_path(), filename),
                                        'grayscale').reshape(ROWS, COLS, CHANNELS)

        return images, masks

    def training_labels(self):
        '''
        Returns a training labels according to the fold we're in
        :return: Training labels
        '''

        return [str(c) for c in self.training_classes]

    def testing_labels(self):
        '''
        Returns a testing labels according to the fold we're in
        :return: Testing labels
        '''

        return [str(c) for c in self.testing_classes]

    def validation_labels(self):
        '''
        Returns a validation labels according to the fold we're in
        :return: Validation labels
        '''

        return [str(c) for c in self.validation_classes]