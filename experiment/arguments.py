'''
Argument parsing module
'''
import argparse

def parse():
    """Parse arguments

    """
    parser = argparse.ArgumentParser(description='Script for PSO experimentation.')

    parser.add_argument('-o',
                        action="store",
                        dest="output_dir",
                        default='out',
                        help='File path to history file for initializing the archive')

    parser.add_argument('-x',
                        action="store",
                        dest="xfold",
                        type=int,
                        help='Index of xvalidation fold')

    parser.add_argument('-e',
                        action="store",
                        dest="nb_epoch",
                        type=int,
                        help='Number of training epochs')

    parser.add_argument('-b',
                        action="store",
                        dest="batch_size",
                        type=int,
                        help='Size of training batch')

    parser.add_argument('-d',
                        action="store",
                        dest="dataset_extension",
                        type=str,
                        default='',
                        help='Dataset extension name')

    parser.add_argument('-r',
                        action="store",
                        dest="rotation",
                        type=int,
                        default=0,
                        help='Rotation range')

    parser.add_argument('--feature-center', dest='center', action='store_true')
    parser.add_argument('--no-feature-center', dest='center', action='store_false')

    parser.add_argument('--feature-normalize', dest='normalize', action='store_true')
    parser.add_argument('--no-feature-normalize', dest='normalize', action='store_false')

    parser.add_argument('--vertical-flip', dest='vertical_flip', action='store_true')
    parser.add_argument('--no-vertical-flip', dest='vertical_flip', action='store_false')

    parser.add_argument('--horizontal-flip', dest='horizontal_flip', action='store_true')
    parser.add_argument('--no-horizontal-flip', dest='horizontal_flip', action='store_false')

    parser.set_defaults(cross_validation=[])

    return parser.parse_args()
