'''
UNet builder module
'''
from typing import Tuple
from keras.layers import Input, Conv2D, UpSampling2D, concatenate, BatchNormalization, MaxPooling2D
import keras.backend as K
from keras.models import Model
from keras.optimizers import Adam

SMOOTH = 1.

def dice_coef(ground_truth, predicted):
    '''
    Calculate dice from input tensors
    :param ground_truth: Tensor with ground truth
    :param predicted: Tensor with prediction
    :return: Dice coefficient as scalar
    '''
    ground_truth = K.flatten(ground_truth)
    prediction = K.flatten(predicted)
    intersection = K.sum(ground_truth * prediction)
    return (2. * intersection + SMOOTH) / (K.sum(ground_truth) + K.sum(prediction) + SMOOTH)


def dice_coef_loss(ground_truth, prediction):
    '''
    Loss function of iverted DICE to be minimized
    :param predicted: Tensor with prediction
    :return: Dice coefficient as scalar
    :return: Negative DICE for loss minimization
    '''
    return -dice_coef(ground_truth, prediction)


def build_unet(input_shape: Tuple[int, int, int]) -> Model:
    '''
    Build a UNet model
    :param input_shape: Input shape of image as tensor shape
    :return: Keras model
    '''
    inputs = Input(input_shape, name='input_00')
    conv1 = Conv2D(20, (3, 3), activation='relu', padding='same')(inputs)

    bn1 = BatchNormalization(axis=1)(conv1)
    conv1_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(bn1)
    bn1 = BatchNormalization(axis=1)(conv1_residual)
    pool1 = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='same')(bn1)

    bn2 = BatchNormalization(axis=1)(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn2)
    bn2 = BatchNormalization(axis=1)(conv2)
    conv2_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(bn2)
    bn2 = BatchNormalization(axis=1)(conv2_residual)
    pool2 = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='same')(bn2)

    bn3 = BatchNormalization(axis=1)(pool2)
    conv3 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn3)
    bn3 = BatchNormalization(axis=1)(conv3)
    conv3_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(bn3)
    bn3 = BatchNormalization(axis=1)(conv3_residual)
    pool3 = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='same')(bn3)

    bn4 = BatchNormalization(axis=1)(pool3)
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn4)
    bn4 = BatchNormalization(axis=1)(conv4)
    conv4_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(bn4)
    bn4 = BatchNormalization(axis=1)(conv4_residual)
    pool4 = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='same')(bn4)

    bn5 = BatchNormalization(axis=1)(pool4)
    conv5 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn5)
    bn5 = BatchNormalization(axis=1)(conv5)
    conv5_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(bn5)
    bn5 = BatchNormalization(axis=1)(conv5_residual)
    pool5 = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='same')(bn5)

    bn6 = BatchNormalization(axis=1)(pool5)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn6)
    bn6 = BatchNormalization(axis=1)(conv6)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn6)
    bn6 = BatchNormalization(axis=1)(conv6)
    up1 = concatenate([UpSampling2D(size=(2, 2))(bn6), conv5_residual], axis=3)

    bn7 = BatchNormalization(axis=1)(up1)
    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn7)
    bn7 = BatchNormalization(axis=1)(conv7)
    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn7)
    bn7 = BatchNormalization(axis=1)(conv7)
    up2 = concatenate([UpSampling2D(size=(2, 2))(bn7), conv4_residual], axis=3)

    bn8 = BatchNormalization(axis=1)(up2)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn8)
    bn8 = BatchNormalization(axis=1)(conv8)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn8)
    bn8 = BatchNormalization(axis=1)(conv8)
    up3 = concatenate([UpSampling2D(size=(2, 2))(bn8), conv3_residual], axis=3)

    bn9 = BatchNormalization(axis=1)(up3)
    conv9 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn9)
    bn9 = BatchNormalization(axis=1)(conv9)
    conv9 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn9)
    bn9 = BatchNormalization(axis=1)(conv9)
    up4 = concatenate([UpSampling2D(size=(2, 2))(bn9), conv2_residual], axis=3)

    bn10 = BatchNormalization(axis=1)(up4)
    conv10 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn10)
    bn10 = BatchNormalization(axis=1)(conv10)
    conv10 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn10)
    bn10 = BatchNormalization(axis=1)(conv10)
    up5 = concatenate([UpSampling2D(size=(2, 2))(bn10), conv1_residual], axis=3)

    bn11 = BatchNormalization(axis=1)(up5)
    conv11 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn11)
    bn11 = BatchNormalization(axis=1)(conv11)
    conv11 = Conv2D(64, (3, 3), activation='relu', padding='same')(bn11)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid', name="output_00")(conv11)

    model = Model(inputs=[inputs], outputs=[conv10])

    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])

    return model

def build_vanilla_unet(input_shape: Tuple[int, int, int]) -> Model:
    '''
    Build a UNet model
    :param input_shape: Input shape of image as tensor shape
    :return: Keras model
    '''
    inputs = Input(input_shape, name='input_00')
    conv1 = Conv2D(20, (3, 3), activation='relu', padding='same')(inputs)

    conv1_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv1_residual)
    maxpool1 = MaxPooling2D()(pool1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(maxpool1)
    conv2_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2_residual)
    maxpool2 = MaxPooling2D()(pool2)

    conv3 = Conv2D(64, (3, 3), activation='relu', padding='same')(maxpool2)
    conv3_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(conv3)
    pool3 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv3_residual)
    maxpool3 = MaxPooling2D()(pool3)

    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(maxpool3)
    conv4_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(conv4)
    pool4 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv4_residual)
    maxpool4 = MaxPooling2D()(pool4)

    conv5 = Conv2D(64, (3, 3), activation='relu', padding='same')(maxpool4)
    conv5_residual = Conv2D(64, (3, 3), activation='relu', padding='same')(conv5)
    pool5 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv5_residual)
    maxpool5 = MaxPooling2D()(pool5)

    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(maxpool5)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv6)
    up1 = concatenate([UpSampling2D(size=(2, 2))(conv6), conv5_residual], axis=3)

    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv7)
    up2 = concatenate([UpSampling2D(size=(2, 2))(conv7), conv4_residual], axis=3)

    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up2)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv8)
    up3 = concatenate([UpSampling2D(size=(2, 2))(conv8), conv3_residual], axis=3)

    conv9 = Conv2D(64, (3, 3), activation='relu', padding='same')(up3)
    conv9 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv9)
    up4 = concatenate([UpSampling2D(size=(2, 2))(conv9), conv2_residual], axis=3)

    conv10 = Conv2D(64, (3, 3), activation='relu', padding='same')(up4)
    conv10 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv10)
    up5 = concatenate([UpSampling2D(size=(2, 2))(conv10), conv1_residual], axis=3)

    conv11 = Conv2D(64, (3, 3), activation='relu', padding='same')(up5)
    conv11 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv11)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid', name="output_00")(conv11)

    model = Model(inputs=[inputs], outputs=[conv10])

    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])

    return model
