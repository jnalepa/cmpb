**Segmenting brain tumors from FLAIR MRI using fully convolutional neural networks**
 
Pablo Ribalta Lorenzo, Jakub Nalepa, Barbara Bobek-Billewicz, Pawel Wawrzyniak, Grzegorz Mrukwa, Michal Kawulok, Pawel Ulrych, Michael P. Hayball

This repository contains the code which shows how to build U-Nets exploited for brain-tumor segmentation in the paper titled "Segmenting brain tumors from FLAIR MRI using fully convolutional neural networks" by Ribalta, Nalepa et al. (currently under review in Computer Methods and Programs in Biomedicine). This code is free to use for research, non-commercial purposes. 

In case of any questions, please contact the corresponding author (Jakub Nalepa).